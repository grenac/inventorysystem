﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemView {

	void SetImage (string spriteName);
	void RefreshCount ();

}
