using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Item))]
public class GroundItem : MonoBehaviour, IItemView {

	[SerializeField]
	private GroundManager _groundManager;
	private bool _mouseIsOver = false;

	// Interface
	public void SetManager (GroundManager manager) {
		_groundManager = manager;
	}

	public void SetImage (string spriteName) {
		GetComponent<SpriteRenderer>().sprite = Managers.SpriteLoader.GetSprite(spriteName);
	}

	public void RefreshCount () {
		// Nothing happens
	}

	// Events
	public void Start () {
		GetComponent<Item>().SetSpriteName(GetComponent<SpriteRenderer>().sprite.name);
	}

	public void OnTriggerStay2D (Collider2D collision) {
		_groundManager.PickUpItem(GetComponent<Item>());
		if (_mouseIsOver == true) {
			_mouseExit();
		}
	}

	public void OnMouseOver () {
		if (Managers.Ui.PointerIsOverUi == false && _mouseIsOver == false) {
			_mouseEnter();
		} else if (Managers.Ui.PointerIsOverUi == true && _mouseIsOver == true)  {
			_mouseExit();
		}
	}

	public void OnMouseExit () {
		if (_mouseIsOver == true) {
			_mouseExit();
		}
	}

	private void _mouseEnter () {
		GetComponent<SpriteRenderer>().color = Color.green;

		Item item = GetComponent<Item>();
		if (item.ItemType != ItemData.ItemType.None) {
			Managers.Ui.ShowItemInfo(item);
		}
		_mouseIsOver = true;
	}

	private void _mouseExit () {
		GetComponent<SpriteRenderer>().color = Color.white;
		Managers.Ui.HideItemInfo();
		_mouseIsOver = false;
	}



}
