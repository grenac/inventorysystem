using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SplitBackground : MonoBehaviour, IPointerClickHandler {
	[SerializeField]
	private SplitManager _splitManager;

	public void OnPointerClick (PointerEventData eventData) {
		_splitManager.HidePanel();
	}
}
