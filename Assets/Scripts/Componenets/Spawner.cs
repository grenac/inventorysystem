using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour, IActivatable {


	[SerializeField]
	private float _period;
	[SerializeField]
	private GameObject _itemToSpawn;
	[SerializeField]
	private GroundManager _groundManager;

	private float _timer;
	private bool _enabled;


	public void Activate () {
		_enabled = true;
	} 

	public void Deactivate () {
		_enabled = false;
	}

	public void Start () {
		_timer = 0;
		_enabled = false;
	}

	public void Update () {
		if (_enabled) {
			_timer += Time.deltaTime;
			if (_timer >= _period) {
				_timer -= _period;
				_spawnItem();
			}
		}
	}

	private void _spawnItem () {
		Vector3 spawnPos = transform.position + Random.insideUnitSphere * 0.3f;
		Instantiate<GameObject>(_itemToSpawn, spawnPos, Quaternion.identity, _groundManager.transform);
	}

}

