using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour {

	[SerializeField]
	private float _damage;

	public void OnTriggerStay2D (Collider2D collision) {
		Player player = collision.gameObject.GetComponent<Player>();
		if (player != null) {
			player.TakeDamage(_damage * Time.deltaTime);
		}
	}
}
