using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Item))]
public class InventoryItem : MonoBehaviour, IItemView {

	[SerializeField]
	private Text _countText;
	[SerializeField]
	private Color _countColor;
	[SerializeField]
	private Color _countColorFull;

	// Interface
	public void SetImage (string spriteName) {
		GetComponent<Image>().sprite = Managers.SpriteLoader.GetSprite(spriteName);
	}

	public void RefreshCount () {
		Item item = GetComponent<Item>();
		if (item.IsStackable() == true) {
			_countText.color = _countColor;
			if (item.HasStackLimit() == true) {
				_countText.text = item.Count.ToString() + "/" + item.MaxCount.ToString();
				if (item.IsAtStackLimit() == true) {
					_countText.color = _countColorFull;
				}
			} else {
				_countText.text = item.Count.ToString();
			}
		} else {
			_countText.text = "";
		}
	}

	// Events
	public void Start () {
		GetComponent<Item>().SetSpriteName(GetComponent<Image>().sprite.name);
	}
}
