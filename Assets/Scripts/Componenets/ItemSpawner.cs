using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour, IActivatable {


	[SerializeField]
	private GroundManager _groundManager;
	[SerializeField]
	private float _period;

	[SerializeField]
	private GameObject _weaponPrefab;
	[SerializeField]
	private GameObject _shieldPrefab;
	[SerializeField]
	private GameObject _helmPrefab;
	[SerializeField]
	private GameObject _bootsPrefab;
	[SerializeField]
	private GameObject _platePrefab;


	private float _timer;
	private bool _enabled;


	public void Activate () {
		_enabled = true;
	}

	public void Deactivate () {
		_enabled = false;
	}

	public void Start () {
		_timer = 0;
		_enabled = false;
	}

	public void Update () {
		if (_enabled) {
			_timer += Time.deltaTime;
			if (_timer >= _period) {
				_timer -= _period;
				_spawnItem();
			}
		}
	}

	private void _spawnItem () {
		GameObject _prefab = _weaponPrefab;
		switch (Random.Range(0, 5)) {
			case 0:
				_prefab = _weaponPrefab;
				break;
			case 1:
				_prefab = _shieldPrefab;
				break;
			case 2:
				_prefab = _helmPrefab;
				break;
			case 3:
				_prefab = _bootsPrefab;
				break;
			case 4:
				_prefab = _platePrefab;
				break;
		}

		Vector3 spawnPos = transform.position + Random.insideUnitSphere * 0.3f;
		GameObject newItem = Instantiate<GameObject>(_prefab, spawnPos, Quaternion.identity, _groundManager.transform);
		newItem.GetComponent<Item>().GenerateRandomBuff(Random.Range(1, 5));
	}

}

