using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GroundClickCatcher : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

	[SerializeField]
	private PointerManager _pointerManager;
	[SerializeField]
	private GroundManager _groundManager;
	
	public void OnPointerClick (PointerEventData eventData) {
		if (_pointerManager.HasItem() == true) {
			Item item = _pointerManager.GetItem();
			_groundManager.CreateItemBehindPlayer(item);
			_pointerManager.RemoveItem();
		}
	}

	public void OnPointerEnter (PointerEventData eventData) {
		Managers.Ui.PointerIsOverUi = false;
	}

	public void OnPointerExit (PointerEventData eventData) {
		Managers.Ui.PointerIsOverUi = true;
	}
}
