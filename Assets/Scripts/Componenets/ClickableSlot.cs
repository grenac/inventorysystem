using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Slot))]
public class ClickableSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

	[SerializeField]
	private SlotManager _slotManager;
	[SerializeField]
	private Color _initialColor;
	[SerializeField]
	private Color _hoverColor;

	// Interface
	public void SetManager (SlotManager slotManager) {
		_slotManager = slotManager;
	}

	// Events
	public void OnPointerClick (PointerEventData eventData) {
		if (eventData.button == PointerEventData.InputButton.Left) {
			if(Managers.Input.IsItemDropKeyDown() == true) {
				_slotManager.DropItem(GetComponent<Slot>().GetItem());
			} else {
				_slotManager.SwitchSlotWithPointer(GetComponent<Slot>());
			}
		} else if (eventData.button == PointerEventData.InputButton.Right) {
			_slotManager.UseItem(GetComponent<Slot>().GetItem());
		} else if (eventData.button == PointerEventData.InputButton.Middle) {
			_slotManager.ConsumeItem(GetComponent<Slot>().GetItem());
		}
	}

	public void OnPointerEnter (PointerEventData eventData) {
		GetComponent<Image>().color = _hoverColor;
		Item itemInSlot = GetComponent<Slot>().GetItem();
		if(itemInSlot.ItemType != ItemData.ItemType.None) {
			Managers.Ui.ShowItemInfo(GetComponent<Slot>().GetItem());
		}
	}

	public void OnPointerExit (PointerEventData eventData) {
		GetComponent<Image>().color = _initialColor;
		Managers.Ui.HideItemInfo();
	}

}
