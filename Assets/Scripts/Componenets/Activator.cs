using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activator : MonoBehaviour {


	[SerializeField]
	private GameObject _target;

	private void OnTriggerEnter2D (Collider2D collision) {
		_target.GetComponent<IActivatable>().Activate();
	}

	private void OnTriggerExit2D (Collider2D collision) {
		_target.GetComponent<IActivatable>().Deactivate();
	}


}
