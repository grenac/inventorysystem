using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour {

	[SerializeField]
	private ItemData _itemData;
	public ItemData ItemData {
		get {
			return _itemData;
		}
		set {
			_itemData = value;
			_refreshView();
		}
	}

	// Interface
	public void SwitchWithItem (Item item) {
		ItemData tempItemData = _itemData;
		_itemData = item.ItemData;
		item.ItemData = tempItemData;
		_refreshView();
	} 

	public void CopyItem (Item item) {
		_itemData = item.ItemData;
		_refreshView();
	}
	
	public void RemoveItem () {
		_itemData = ItemData.None;
		_refreshView();
	}

	public void ConsumeItem () {
		if (IsStackable() == true) {
			_itemData.CurrentNum--;
			if (_itemData.CurrentNum == 0) {
				RemoveItem();
			} else {
				_refreshView();
			}
		} else {
			RemoveItem();
		}
	}

	public void SetSpriteName (string name) {
		_itemData.SpriteName = name;
		_refreshView();
	}

	public void MergeStackable (Item item) {
		int numToFull = _itemData.MaxNum - _itemData.CurrentNum;
		if (item.Count <= numToFull) {
			_itemData.CurrentNum += item.Count;
			item.RemoveItem();
		} else {
			_itemData.CurrentNum += numToFull;
			item.SetCount(item.Count - numToFull);
		}
		item._refreshView();
		_refreshView();
	}

	public bool IsStackable() {
		return _itemData.MaxNum != 0;
	}

	public bool HasStackLimit () {
		return _itemData.MaxNum != int.MaxValue;
	}

	public bool IsAtStackLimit () {
		return _itemData.CurrentNum == _itemData.MaxNum;
	}

	public ItemData.ItemType ItemType {
		get {
			return _itemData.Type;
		}
	}

	public int Count {
		get {
			return _itemData.CurrentNum;
		}
	}

	public int MaxCount {
		get {
			return _itemData.MaxNum;
		}
	}

	public string Name {
		get {
			return _itemData.Name;
		}
	}

	public string Desc {
		get {
			return _itemData.Desc;
		}
	}

	public int CurrentDurability {
		get {
			return _itemData.CurrentDurability;
		}
	}

	public int MaxDurability {
		get {
			return _itemData.MaxDurability;
		}
	}

	public Buff Buff {
		get {
			return _itemData.buff;
		}
	}

	public void GenerateRandomBuff(int num) {
		Buff buff = new Buff(Random.Range(int.MinValue, int.MaxValue).ToString());
		for (int i = 0; i < num; i++) {
			int val = Random.Range(1, 100);
			switch (Random.Range(0, 7)) {
				case 0:
					buff.Str += val;
					break;
				case 1:
					buff.Dex += val;
					break;
				case 2:
					buff.Con += val;
					break;
				case 3:
					buff.Int += val;
					break;
				case 4:
					buff.Dmg += val;
					break;
				case 5:
					buff.Acc += val;
					break;
				case 6:
					buff.Hp += val;
					break;
				case 7:
					buff.Mana += val;
					break;
				default:
					break;
			}
		}
		_itemData.buff += buff;
	}

	public void Degrade () {
		if (_itemData.CurrentDurability > 0) {
			_itemData.CurrentDurability--;
		}
	}

	public void SetCount (int newCount) {
		_itemData.CurrentNum = Mathf.Min(newCount, _itemData.MaxNum);
		_refreshView();
	}

	// Internal
	private void _refreshView () {
		GetComponent<IItemView>().SetImage(_itemData.SpriteName);
		GetComponent<IItemView>().RefreshCount();
	}

}
