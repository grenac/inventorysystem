using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	[SerializeField]
	private EquipManager _equipManager;
	[SerializeField]
	private AttributesManager _attributesManager;
	[SerializeField]
	private float MoveAcceleration;
	[SerializeField]
	private float MoveAccelerationMana;
	[SerializeField]
	private float MoveManaCost;
	[SerializeField]
	private float MoveDrag;

	private float _totalDistanceMoved;

	public void TakeDamage (float damage) {
		_attributesManager.LowerHp(damage);
	}

	public void Start () {
		_totalDistanceMoved = 0f;
	}

	public void FixedUpdate () {
		float acc = MoveAcceleration;
		if (Managers.Input.InputVector.magnitude != 0 && _attributesManager.SpendMana(MoveManaCost * Time.deltaTime)) {
			acc = MoveAccelerationMana;
		}

		Rigidbody2D rb = GetComponent<Rigidbody2D>();
		// apply drag
		rb.velocity *= Mathf.Exp((1 - MoveDrag) * Time.deltaTime);
		// apply acceleration
		rb.velocity += acc * Managers.Input.InputVector * Time.deltaTime;

		_totalDistanceMoved += rb.velocity.magnitude * Time.deltaTime;
		if (_totalDistanceMoved >= 10f) {
			Managers.Analitics.LogPlayerMoved10();
			_equipManager.DegradeEquipement();
			_totalDistanceMoved -= 10f;
		}
	}

	public void Update () {
		Vector2 vel = GetComponent<Rigidbody2D>().velocity;
		bool isWalking = false;
		int direction = 0;

		if (Mathf.Abs(vel.x) >= Mathf.Abs(vel.y)) {
			if (vel.x > 0f) {
				direction = 1;
			} else {
				direction = 3;
			}
		} else {
			if (vel.y > 0f) {
				direction = 0;
			} else {
				direction = 2;
			}
		}

		if (vel.magnitude > 0.5f) {
			isWalking = true;
		} else {
			direction = -1;
		}

		GetComponent<Animator>().SetBool("IsWalking", isWalking);
		GetComponent<Animator>().SetInteger("Direction", direction);

		GetComponent<Animator>().speed = Mathf.Max(0.35f, vel.magnitude / 1.5f);
	}

	private void OnCollisionEnter2D (Collision2D collision) {
		Managers.Analitics.LogPlayerCollided();
	}


}
