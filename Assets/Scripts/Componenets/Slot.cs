using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour {

	[SerializeField]
	private Item _itemObject;
	[SerializeField]
	private ItemData.ItemType _slotType;
	
	// Interface
	public bool CanHold (ItemData.ItemType type) {
		return _slotType == ItemData.ItemType.None || _slotType == type;
	}
	
	public void SetItem (Item item) {
		_itemObject.CopyItem(item);
	}

	public void RemoveItem () {
		_itemObject.RemoveItem();
	}
	
	public Item GetItem() {
		return _itemObject;
	}

	public bool HasItem () {
		return _itemObject.ItemType != ItemData.ItemType.None;
	}
	
}
