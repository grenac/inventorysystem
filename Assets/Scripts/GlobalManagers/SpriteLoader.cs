using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteLoader : MonoBehaviour {

	[SerializeField]
	private string _pathToLoad;
	[SerializeField]
	private Sprite _defaultSprite;

	private Dictionary<string, Sprite> _spriteDict;

	void Start () {
		Sprite[] sprites = Resources.LoadAll<Sprite>(_pathToLoad);
		_spriteDict = new Dictionary<string, Sprite>();
		foreach (Sprite sprite in sprites) {
			_spriteDict.Add(sprite.name, sprite);
		}
	}

	public Sprite GetSprite (string spriteName) {
		if (spriteName != null && _spriteDict.ContainsKey(spriteName)) {
			return _spriteDict[spriteName];
		} else {
			return _defaultSprite;
		}
	}

}
