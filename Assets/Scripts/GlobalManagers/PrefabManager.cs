using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class PrefabManager : MonoBehaviour {

    [SerializeField]
    public GameObject InventoryItem;
    [SerializeField]
    public GameObject GroundItem;
    [SerializeField]
    public GameObject PointerItem;
    [SerializeField]
    public GameObject EquipItem;
	[SerializeField]
	public GameObject InventorySlot;

}

