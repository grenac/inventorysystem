using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

	[SerializeField]
	private KeyCode _inventoryKey;
	[SerializeField]
	private KeyCode _equipementKey;
	[SerializeField]
	private KeyCode _attributesKey;
	[SerializeField]
	private KeyCode _itemDropKey;

	private Vector2 _inputVector;
	public Vector2 InputVector {
		get {
			return _inputVector;
		}
	}

	public bool IsItemDropKeyDown () {
		return Input.GetKey(_itemDropKey);
	}

	public void Start () {
		_inputVector = Vector2.zero;
	}
	
	public void Update () {
		float horizontal = Input.GetAxisRaw("Horizontal");
		float vertical = Input.GetAxisRaw("Vertical");
		_inputVector = new Vector2(horizontal, vertical);
		_inputVector.Normalize();

		if (Input.GetKeyDown(_inventoryKey) == true) {
			Managers.Ui.ToggleItemsPanel();
		}
		if (Input.GetKeyDown(_equipementKey) == true) {
			Managers.Ui.ToggleEquipPanel();
		}
		if (Input.GetKeyDown(_attributesKey) == true) {
			Managers.Ui.ToggleAttributesPanel();
		}
	}

}
