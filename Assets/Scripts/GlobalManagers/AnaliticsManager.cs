using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnaliticsManager : MonoBehaviour {

	[SerializeField]
	private int _maxLogPlayerMoved10Count;
	private int _logPlayerMoved10Count;

	[SerializeField]
	private float _logPlayerCollidedDelay;
	private float _logPlayerCollidedTime;


	// Interface
	public void LogItemPickup (string itemName, ItemData.ItemType itemType) {
		Debug.Log("AnaliticsManager - LogItemPickup(" + itemName + ", " + itemType + ")");
		Dictionary<string, object> AnalData = new Dictionary<string, object>();
		AnalData.Add("itemName", itemName);
		AnalData.Add("itemType", itemType);
		Analytics.CustomEvent("ItemPickup", AnalData);
	}

	public void LogItemEquiped (string itemName, ItemData.ItemType itemType) {
		Debug.Log("AnaliticsManager - LogItemEquiped(" + itemName + ", " + itemType + ")");
		Dictionary<string, object> AnalData = new Dictionary<string, object>();
		AnalData.Add("itemName", itemName);
		AnalData.Add("itemType", itemType);
		Analytics.CustomEvent("ItemEquiped", AnalData);
	}

	public void LogItemConsumed (string itemName, ItemData.ItemType itemType) {
		Debug.Log("AnaliticsManager - LogItemConsumed(" + itemName + ", " + itemType + ")");
		Dictionary<string, object> AnalData = new Dictionary<string, object>();
		AnalData.Add("itemName", itemName);
		AnalData.Add("itemType", itemType);
		Analytics.CustomEvent("ItemConsumed", AnalData);
	}

	public void LogPanelOpened (string panelType) {
		Debug.Log("AnaliticsManager - LogPanelOpened(" + panelType + ")");
		Dictionary<string, object> AnalData = new Dictionary<string, object>();
		AnalData.Add("panelType", panelType);
		Analytics.CustomEvent("PanelOpened", AnalData);
	}

	public void LogPlayerMoved10 () {
		if (_logPlayerMoved10Count < _maxLogPlayerMoved10Count) {
			Debug.Log("AnaliticsManager - LogPlayerMoved10");
			Dictionary<string, object> AnalData = new Dictionary<string, object>();
			Analytics.CustomEvent("PlayerMoved10", AnalData);
			_logPlayerMoved10Count++;
		}
	}

	public void LogPlayerCollided () {
		if (_logPlayerCollidedTime <= 0) {
			Debug.Log("AnaliticsManager - LogPlayerCollided");
			Dictionary<string, object> AnalData = new Dictionary<string, object>();
			Analytics.CustomEvent("PlayerCollide", AnalData);
			_logPlayerCollidedTime = _logPlayerCollidedDelay;
		}
	}

	// Events
	public void Start () {
		_logPlayerMoved10Count = 0;
		_logPlayerCollidedTime = 0f;
	}

	public void Update () {
		_logPlayerCollidedTime -= Time.deltaTime;
		_logPlayerCollidedTime = Mathf.Max(_logPlayerCollidedTime, 0);
	}

}
