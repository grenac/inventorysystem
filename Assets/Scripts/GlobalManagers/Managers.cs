using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Managers {

	private static SpriteLoader _spriteLoader;
	public static SpriteLoader SpriteLoader {
		get {
			return _spriteLoader;
		}
	}

	private static InputManager _inputManager;
	public static InputManager Input {
		get {
			return _inputManager;
		}
	}

	private static PrefabManager _prefabManager;
	public static PrefabManager Prefab {
		get {
			return _prefabManager;
		}
	}

	private static UiManager _uiManager;
	public static UiManager Ui {
		get {
			return _uiManager;
		}
	}

	private static AnaliticsManager _analiticsManager;
	public static AnaliticsManager Analitics {
		get {
			return _analiticsManager;
		}
	}


	public static void Start () {
		GameObject managersObject = GameObject.FindGameObjectWithTag("Managers");
		_uiManager = managersObject.GetComponent<UiManager>();
		_spriteLoader = managersObject.GetComponent<SpriteLoader>();
		_inputManager = managersObject.GetComponent<InputManager>();
		_prefabManager = managersObject.GetComponent<PrefabManager>();
		_analiticsManager = managersObject.GetComponent<AnaliticsManager>();
	}
}
