using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour {

	[SerializeField]
	private GameObject _equipPanel;
	[SerializeField]
	private GameObject _itemsPanel;
	[SerializeField]
	private GameObject _attributesPanel;
	[SerializeField]
	private InfoManager _infoPanel;
	[SerializeField]
	private SplitManager _splitPanel;

	[SerializeField]
	private GameObject _equipPanelButton;
	[SerializeField]
	private GameObject _itemsPanelButton;
	[SerializeField]
	private GameObject _attributesPanelButton;

	private bool _attributesPanelVisible;
	private bool _itemsPanelVisible;
	private bool _equipPanelVisible;

	public bool PointerIsOverUi;

	public void ToggleEquipPanel () {
		RectTransform rt = _equipPanel.GetComponent<RectTransform>();
		if (_equipPanelVisible == true) {
			rt.anchoredPosition = new Vector2(rt.sizeDelta.x, 0);
			_equipPanelButton.SetActive(true);
			_equipPanelVisible = false;
		} else {
			rt.anchoredPosition = new Vector2(0, 0);
			_equipPanelButton.SetActive(false);
			_equipPanelVisible = true;
			Managers.Analitics.LogPanelOpened("Equip");
		}
	}

	public void ToggleItemsPanel () {
		RectTransform rt = _itemsPanel.GetComponent<RectTransform>();
		if (_itemsPanelVisible == true) {
			rt.anchoredPosition = new Vector2(rt.sizeDelta.x, 0);
			_itemsPanelButton.SetActive(true);
			_itemsPanelVisible = false;
		} else {
			rt.anchoredPosition = new Vector2(0, 0);
			_itemsPanelButton.SetActive(false);
			_itemsPanelVisible = true;
			Managers.Analitics.LogPanelOpened("Items");
		}
	}

	public void ToggleAttributesPanel () {
		RectTransform rt = _attributesPanel.GetComponent<RectTransform>();
		if (_attributesPanelVisible == true) {
			rt.anchoredPosition = new Vector2(-rt.sizeDelta.x, 0);
			_attributesPanelButton.SetActive(true);
			_attributesPanelVisible = false;
		} else {
			rt.anchoredPosition = new Vector2(0, 0);
			_attributesPanelButton.SetActive(false);
			_attributesPanelVisible = true;
			Managers.Analitics.LogPanelOpened("Attributes");
		}
	}

	public void ShowItemInfo (Item item) {
		_infoPanel.ShowItemInfo(item);
	}

	public void HideItemInfo () {
		_infoPanel.HideItemInfo();
	}

	public void ShowSplitPanel () {
		_splitPanel.ShowPanel();
	}

	public void HideSplitPanel () {
		_splitPanel.HidePanel();
	}

	public void Start () {
		_attributesPanelVisible = true;
		_itemsPanelVisible = true;
		_equipPanelVisible = true;
	}

}
