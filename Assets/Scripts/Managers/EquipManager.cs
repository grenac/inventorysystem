using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipManager : SlotManager {

	[SerializeField]
	private AttributesManager _attributesManager;
	[SerializeField]
	private InventoryManager _inventoryManager;
	[SerializeField]
	private GroundManager _groundManager;

	[SerializeField]
	private Slot _headSlot;
	[SerializeField]
	private Slot _bodySlot;
	[SerializeField]
	private Slot _feetSlot;
	[SerializeField]
	private Slot _weaponSlot;
	[SerializeField]
	private Slot _shieldSlot;

	public override void UseItem (Item item) {
		_inventoryManager.AddItemToInventory(item);
		_removeBuff(item);
		item.RemoveItem();
	}

	public override void SwitchSlotWithPointer (Slot slot) {
		Item pointerItem = _pointerManager.GetItem();
		Item slotItem = slot.GetItem();
		if (pointerItem.ItemType == ItemData.ItemType.None || slot.CanHold(pointerItem.ItemType)) {
			_removeBuff(slotItem);
			_addBuff(pointerItem);
			slotItem.SwitchWithItem(pointerItem);
		}
		Managers.Analitics.LogItemEquiped(slotItem.Name, slotItem.ItemType);
	}

	public override void DropItem (Item item) {
		_groundManager.CreateItemBehindPlayer(item);
		item.RemoveItem();
	}

	public bool IsSlotEmpty(ItemData.ItemType itemType) {
		switch (itemType) {
			case ItemData.ItemType.Head:
				return _headSlot.GetItem().ItemType == ItemData.ItemType.None;
			case ItemData.ItemType.Body:
				return _bodySlot.GetItem().ItemType == ItemData.ItemType.None;
			case ItemData.ItemType.Feet:
				return _feetSlot.GetItem().ItemType == ItemData.ItemType.None;
			case ItemData.ItemType.Weapon:
				return _weaponSlot.GetItem().ItemType == ItemData.ItemType.None;
			case ItemData.ItemType.Shield:
				return _shieldSlot.GetItem().ItemType == ItemData.ItemType.None;
			default:
				return false;
		}
	}

	public void DegradeEquipement () {
		_headSlot.GetItem().Degrade();
		if (_headSlot.GetItem().CurrentDurability == 0) {
			UseItem(_headSlot.GetItem());
		}
		_bodySlot.GetItem().Degrade();
		if (_bodySlot.GetItem().CurrentDurability == 0) {
			UseItem(_bodySlot.GetItem());
		}
		_feetSlot.GetItem().Degrade();
		if (_feetSlot.GetItem().CurrentDurability == 0) {
			UseItem(_feetSlot.GetItem());
		}
		_weaponSlot.GetItem().Degrade();
		if (_weaponSlot.GetItem().CurrentDurability == 0) {
			UseItem(_weaponSlot.GetItem());
		}
		_shieldSlot.GetItem().Degrade();
		if (_shieldSlot.GetItem().CurrentDurability == 0) {
			UseItem(_shieldSlot.GetItem());
		}
	}

	public void SwitchWithEquipItem (Item item) {
		Managers.Analitics.LogItemEquiped(item.Name, item.ItemType);

		switch (item.ItemType) {
			case ItemData.ItemType.Head:
				_removeBuff(_headSlot.GetItem());
				_addBuff(item);
				item.SwitchWithItem(_headSlot.GetItem());
				break;
			case ItemData.ItemType.Body:
				_removeBuff(_bodySlot.GetItem());
				_addBuff(item);
				item.SwitchWithItem(_bodySlot.GetItem());
				break;
			case ItemData.ItemType.Feet:
				_removeBuff(_feetSlot.GetItem());
				_addBuff(item);
				item.SwitchWithItem(_feetSlot.GetItem());
				break;
			case ItemData.ItemType.Weapon:
				_removeBuff(_weaponSlot.GetItem());
				_addBuff(item);
				item.SwitchWithItem(_weaponSlot.GetItem());
				break;
			case ItemData.ItemType.Shield:
				_removeBuff(_shieldSlot.GetItem());
				_addBuff(item);
				item.SwitchWithItem(_shieldSlot.GetItem());
				break;
			default:
				break;
		}
	}

	// Events
	public void Start () {
		Managers.Ui.ToggleEquipPanel();
	}

	// internal
	private void _addBuff (Item item) {
		if (item.ItemType != ItemData.ItemType.None) {
			_attributesManager.AddBuff(item.Buff);
		}
	}

	private void _removeBuff (Item item) {
		if (item.ItemType != ItemData.ItemType.None) {
			_attributesManager.RemoveBuff(item.Buff);
		}
	}

}
