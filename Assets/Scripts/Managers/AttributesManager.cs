using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttributesManager : MonoBehaviour {

	[SerializeField]
	private Buff _baseStats;
	[SerializeField]
	private Text _textObject;

	[SerializeField]
	private float _dmgPerStr;
	[SerializeField]
	private float _hpPerCon;
	[SerializeField]
	private float _accPerDex;
	[SerializeField]
	private float _manaPerInt;

	[SerializeField]
	private float _currentMana;
	[SerializeField]
	private float _currentHp;

	private Dictionary<string, Buff> _activeBuffs;
	private Buff _totalStats;

	// Interface
	public void AddBuff (Buff buff) {
		if (_activeBuffs.ContainsKey(buff.Id) == true) {
			_activeBuffs[buff.Id] = buff.Copy();
		} else {
			_activeBuffs.Add(buff.Id, buff.Copy());
			_recalcBuffs();
		}
	}

	public void RemoveBuff (Buff buff) {
		_activeBuffs.Remove(buff.Id);
		_recalcBuffs();
	}

	public void ClearBuffs () {
		_activeBuffs.Clear();
		_recalcBuffs();
	}

	public bool SpendMana(float amount) {
		if (_currentMana >= amount) {
			_currentMana -= amount;
			return true;
		}
		return false;
	}

	public void LowerHp (float amount) {
		_currentHp -= amount;
		_currentHp = Mathf.Max(0, _currentHp);
	}

	public int Str {
		get {
			return _totalStats.Str;
		}
	}
	public int Dex {
		get {
			return _totalStats.Dex;
		}
	}
	public int Con {
		get {
			return _totalStats.Con;
		}
	}
	public int Int {
		get {
			return _totalStats.Int;
		}
	}
	public float Dmg {
		get {
			return _totalStats.Dmg + _totalStats.Str * _dmgPerStr;
		}
	}
	public float Acc {
		get {
			return _totalStats.Acc + _totalStats.Dex * _accPerDex;
		}
	}
	public float Hp {
		get {
			return _totalStats.Hp + _totalStats.Con * _hpPerCon;
		}
	}
	public float Mana {
		get {
			return _totalStats.Mana + _totalStats.Int * _manaPerInt;
		}
	}
	public float ManaRegen {
		get {
			return _totalStats.ManaRegen;
		}
	}
	public float HpRegen {
		get {
			return _totalStats.HpRegen;
		}
	}
	public float CurrentMana {
		get {
			return _currentMana;
		}
	}
	public float CurrentHp {
		get {
			return _currentHp;
		}
	}

	// Events
	public void Start () {
		_activeBuffs = new Dictionary<string, Buff>();
		AddBuff(_baseStats);
		Managers.Ui.ToggleAttributesPanel();
	}

	public void Update () {
		// degrade buffs and remove done ones
		List<Buff> buffsToRemove = new List<Buff>();
		foreach (Buff buff in _activeBuffs.Values) {
			if (buff.Duration != 0f) {
				buff.Duration -= Time.deltaTime;
				if (buff.Duration <= 0) {
					// don't modify the dict while looping through it
					buffsToRemove.Add(buff);
				}
			}
		}
		foreach (Buff buff in buffsToRemove) {
			RemoveBuff(buff);
		}

		// apply mana and health regen
		_currentMana += ManaRegen * Time.deltaTime;
		_currentHp += HpRegen * Time.deltaTime;

		// check current mana and health
		_currentMana = Mathf.Min(_currentMana, Mana);
		_currentHp = Mathf.Min(_currentHp, Hp);

		_updateText();
	}

	// Internal
	private void _recalcBuffs () {
		_totalStats = new Buff("totalStats");
		foreach (Buff buff in _activeBuffs.Values) {
			_totalStats += buff;
		}
		_updateText();
	}

	private void _updateText () {
		_textObject.text = "";
		_textObject.text += "STR: " + Str.ToString() + "\n";
		_textObject.text += "DEX: " + Dex.ToString() + "\n";
		_textObject.text += "CON: " + Con.ToString() + "\n";
		_textObject.text += "INT: " + Int.ToString() + "\n";
		_textObject.text += "\n";
		_textObject.text += "DMG: " + Dmg.ToString("F0") + "\n";
		_textObject.text += "ACC: " + Acc.ToString("F0") + "\n";
		_textObject.text += "\n";
		_textObject.text += "HP: " + CurrentHp.ToString("F0") + "/" + Hp.ToString("F0") + "\n";
		_textObject.text += "MANA: " + CurrentMana.ToString("F0") + "/" + Mana.ToString("F0") + "\n";
		_textObject.text += "\n";
		_textObject.text += "MANA REGEN: " + ManaRegen.ToString("F0") + "\n";
		_textObject.text += "HP REGEN: " + HpRegen.ToString("F0") + "\n";
	}

}
