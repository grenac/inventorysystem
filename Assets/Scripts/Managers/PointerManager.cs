using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerManager : MonoBehaviour {

	[SerializeField]
	private Slot _slot;

	public Item SwitchItem (Item item) {
		Item oldItem = _slot.GetItem();
		_slot.SetItem(item);
		return oldItem;
	}

	public void SetItem (Item item) {
		_slot.SetItem(item);
	}

	public Item GetItem () {
		return _slot.GetItem();
	}

	public void RemoveItem () {
		_slot.RemoveItem();
	}

	public bool HasItem() {
		return _slot.HasItem();
	}

	void Update () {
		_slot.transform.position = Input.mousePosition;
	}

}
