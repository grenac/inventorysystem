using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplitManager : MonoBehaviour {

	[SerializeField]
	private PointerManager _pointerManager;
	[SerializeField]
	private Slider _slider;
	[SerializeField]
	private InputField _inputField;
	[SerializeField]
	private GameObject _background;

	private int _limit = 1;
	private int _val;
	private Item _item;

	public void SetItem (Item item) {
		_item = item;
		_limit = _item.Count - 1;
		_slider.minValue = 1;
		_slider.maxValue = _limit;
		_val = (_limit + 1) / 2;
	}

	public void ShowPanel () {
		_background.SetActive(true);
		_refreshView();
	}

	public void HidePanel () {
		_background.SetActive(false);
	}

	public void OnSliderChange () {
		_val = (int)_slider.value;
		// _refreshView happens in Update to avoid recursion
	}

	public void OnInputChange () {
		_val = int.Parse(_inputField.text);
		_val = Mathf.Clamp(_val, 1, _limit);
		// _refreshView happens in Update to avoid recursion
	}

	public void Increase () {
		_val++;
		_val = Mathf.Clamp(_val, 1, _limit);
	}

	public void Decrease () {
		_val--;
		_val = Mathf.Clamp(_val, 1, _limit);
	}

	public void Cancel () {
		HidePanel();
	}

	public void InitiateSplit () {
		Item pointerItem = _pointerManager.GetItem();
		pointerItem.CopyItem(_item);
		pointerItem.SetCount(_val);
		_item.SetCount(_limit - _val + 1);
		HidePanel();
	}

	public void Update () {
		_refreshView();
	} 

	// Internal 
	private void _refreshView () {
		_inputField.text = _val.ToString();
		_slider.value = _val;
	}

}
