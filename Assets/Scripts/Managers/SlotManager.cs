using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotManager : MonoBehaviour {

	[SerializeField]
	protected PointerManager _pointerManager;

	public virtual void SwitchSlotWithPointer (Slot slot) { }
	public virtual void UseItem (Item item) { }
	public virtual void DropItem (Item item) { }
	public virtual void ConsumeItem (Item item) { }
}
