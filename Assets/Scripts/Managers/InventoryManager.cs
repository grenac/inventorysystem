using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : SlotManager {

	[SerializeField]
	private EquipManager _equipManager;
	[SerializeField]
	private AttributesManager _attributesManager;
	[SerializeField]
	private SplitManager _splitManager;
	[SerializeField]
	private GroundManager _groundManager;
	[SerializeField]
	private int _numColumns;
	[SerializeField]
	private int _minRows;

	private List<Slot> _slots;

	// Interface
	public override void SwitchSlotWithPointer (Slot slot) {
		Item pointerItem = _pointerManager.GetItem();
		Item slotItem = slot.GetItem();
		if (pointerItem.ItemType == ItemData.ItemType.None || slot.CanHold(pointerItem.ItemType)) {
			if (slotItem.Name == pointerItem.Name && slotItem.IsStackable() == true) {
				slotItem.MergeStackable(pointerItem);
			} else {
				slotItem.SwitchWithItem(pointerItem);
			}
		}
	}

	public override void UseItem (Item item) {
		if (_pointerManager.GetItem().ItemType == ItemData.ItemType.None) {
			if (item.IsStackable() == true && item.Count >= 2) {
				_splitManager.SetItem(item);
				_splitManager.ShowPanel();
			} else {
				_equipManager.SwitchWithEquipItem(item);
			}
		}
	}

	public override void DropItem (Item item) {
		_groundManager.CreateItemBehindPlayer(item);
		item.RemoveItem();
	}

	public override void ConsumeItem (Item item) {
		if (item.ItemType == ItemData.ItemType.Usable || item.ItemType == ItemData.ItemType.Powerup) {
			Managers.Analitics.LogItemConsumed(item.Name, item.ItemType);
			_attributesManager.AddBuff(item.Buff);
			item.ConsumeItem();
			_checkSlots();
		}
	}

	public void AddItemToInventory (Item item) {
		if (item.ItemType == ItemData.ItemType.Powerup) {
			ConsumeItem(item);
		} else if (_equipManager.IsSlotEmpty(item.ItemType)) {
			_equipManager.SwitchWithEquipItem(item);
		} else {
			if (item.IsStackable()) {
				foreach (Slot slot in _slots) {
					Item itemInSlot = slot.GetItem();
					if (itemInSlot.Name == item.Name) {
						itemInSlot.MergeStackable(item);
						if (item.ItemType == ItemData.ItemType.None) {
							return;
						}
					}
				}
			}
			_getFirstFreeInventorySlot().SetItem(item);
			_checkSlots();
		}
	}

	// Events
	public void Start () {
		_slots = new List<Slot>();
		// It is neccesary for this to iterate through children in the order as they are in the editor, top to bottom
		// It works like this now but it is not a documented feature and might change at any time.
		foreach (Slot slot in GetComponentsInChildren<Slot>()) {
			_slots.Add(slot);
		}
		_checkSlots();
		Managers.Ui.ToggleItemsPanel();
	}

	public void Update () {
		// TODO: make a OnChanged event and only check slots OnChange
		_checkSlots();
	}

	// Internal
	private void _checkSlots () {
		bool needsCheck = true;
		while (needsCheck == true) {
			needsCheck = false;
			int numFilledSlots = 0;
			int maxFilledSlotIndex = 0;

			for (int i = 0; i < _slots.Count; i++) {
				if (_slots[i].HasItem() == true) {
					numFilledSlots++;
					maxFilledSlotIndex = i;
				}
			}

			if (_slots.Count > _minRows * _numColumns // is not below minumum number of rows
				&& maxFilledSlotIndex < _slots.Count - _numColumns // and last row does not contain any items
				&& numFilledSlots < _slots.Count - _numColumns // and there is at least one other empty slot (so that the removed row does not get auto added)
				) {
				_removeRow();
				needsCheck = true;
			}

			if (_slots.Count < _minRows * _numColumns // is below minimum number of rows
				|| numFilledSlots == _slots.Count // inventory is full
				) {
				_addRow();
				needsCheck = true;
			}
		}
	}

	private void _removeRow () {
		int removeStartIndex = _slots.Count - _numColumns;
		for (int i = removeStartIndex; i < _slots.Count; i++) {
			Destroy(_slots[i].gameObject);
		}
		_slots.RemoveRange(removeStartIndex, _numColumns);
	}

	private void _addRow () {
		int limit = _numColumns - ( _slots.Count % _numColumns ); // prevents adding a non full row
		for (int i = 0; i < limit; i++) {
			GameObject newSlotObject = Instantiate<GameObject>(Managers.Prefab.InventorySlot, transform);
			newSlotObject.GetComponent<RectTransform>().localScale = Vector2.one; // for some reason object has random scale when Instantiated
			newSlotObject.GetComponent<ClickableSlot>().SetManager(GetComponent<SlotManager>());
			_slots.Add(newSlotObject.GetComponent<Slot>());
		}
	}

	private Slot _getFirstFreeInventorySlot () {
		foreach (Slot slot in _slots) {
			if (slot.HasItem() == false) {
				return slot;
			}
		}
		return null;
	}
}
