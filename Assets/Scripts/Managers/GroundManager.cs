using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundManager : MonoBehaviour {

	[SerializeField]
	private InventoryManager _inventoryManager;
	[SerializeField]
	private GameObject _player;
	[SerializeField]
	private GameObject _groundItemPrefab;
	[SerializeField]
	private float _dropItemDistance;
	[SerializeField]
	private float _dropItemVariation;

	public void PickUpItem (Item groundItem) {
		Managers.Analitics.LogItemPickup(groundItem.Name, groundItem.ItemType);

		_inventoryManager.AddItemToInventory(groundItem);
		Destroy(groundItem.gameObject);
	}

	public void CreateItemBehindPlayer (Item item) {
		Vector2 playerVelocity = _player.GetComponent<Rigidbody2D>().velocity;
		Vector2 behindPLayer;
		if (playerVelocity == Vector2.zero) {
			behindPLayer = Vector2.down;
		} else {
			behindPLayer = -playerVelocity.normalized;
		}

		Vector2 newItemOffset = behindPLayer * _dropItemDistance + Random.insideUnitCircle * _dropItemVariation;
		Vector3 newItemPosition = new Vector3(
			_player.transform.position.x + newItemOffset.x,
			_player.transform.position.y + newItemOffset.y,
			_player.transform.position.z);

		GameObject newGroundItemObject = Instantiate(_groundItemPrefab, newItemPosition, Quaternion.identity, transform);
		newGroundItemObject.GetComponent<Item>().CopyItem(item);
		newGroundItemObject.GetComponent<GroundItem>().SetManager(this);
	}

}
