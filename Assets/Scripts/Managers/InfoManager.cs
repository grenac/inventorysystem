using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoManager : MonoBehaviour {

	[SerializeField]
	public Text _text;

	public void ShowItemInfo (Item item) {
		gameObject.SetActive(true);

		_text.text = "";

		if (item.Count > 1) {
			_text.text += item.Count.ToString() + " x " + item.Name + "\n";
		} else {
			_text.text += item.Name + "\n";
		}

		_text.text += "---------------------\n";

		if (item.Buff.Str != 0) {
			_text.text += " + " + item.Buff.Str.ToString() + " Str\n";
		}
		if (item.Buff.Dex != 0) {
			_text.text += " + " + item.Buff.Dex.ToString() + " Dex\n";
		}
		if (item.Buff.Con != 0) {
			_text.text += " + " + item.Buff.Con.ToString() + " Con\n";
		}
		if (item.Buff.Int != 0) {
			_text.text += " + " + item.Buff.Int.ToString() + " Int\n";
		}
		if (item.Buff.Dmg != 0) {
			_text.text += " + " + item.Buff.Dmg.ToString("F0") + " Dmg\n";
		}
		if (item.Buff.Acc != 0) {
			_text.text += " + " + item.Buff.Acc.ToString("F0") + " Acc\n";
		}
		if (item.Buff.Hp != 0) {
			_text.text += " + " + item.Buff.Hp.ToString("F0") + " Hp\n";
		}
		if (item.Buff.Mana != 0) {
			_text.text += " + " + item.Buff.Mana.ToString("F0") + " Mana\n";
		}
		if (item.Buff.ManaRegen != 0) {
			_text.text += " + " + item.Buff.ManaRegen.ToString("F0") + " Mana Regen\n";
		}
		if (item.Buff.HpRegen != 0) {
			_text.text += " + " + item.Buff.HpRegen.ToString("F0") + " Hp Regen\n";
		}
		if (item.Buff.Duration != 0) {
			_text.text += "for " + item.Buff.Duration.ToString("F0") + " seconds\n";
		}

		_text.text += "---------------------\n";

		_text.text += item.Desc + "\n";

		if (item.MaxDurability != 0) {
			_text.text += "---------------------\n";
			_text.text += "Durability: " + item.CurrentDurability.ToString() + "/" + item.MaxDurability.ToString() + "\n";
		}
	}

	public void HideItemInfo () {
		gameObject.SetActive(false);
	}

}
