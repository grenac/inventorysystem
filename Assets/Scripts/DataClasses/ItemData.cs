using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ItemData {

	public enum ItemType {None, Weapon, Shield, Head, Body, Feet, Usable, Powerup, Currency};

	public static ItemData None = new ItemData {
		Name = "none",
		Desc = "",
		SpriteName = "defaultItem",
		Type = ItemType.None,
		MaxNum = 0,
		CurrentNum = 0,
		MaxDurability = int.MaxValue,
		CurrentDurability = int.MaxValue,
		buff = new Buff("none"),
	};

    public string Name;
	public string Desc;

	[HideInInspector]
	public string SpriteName;

	public ItemType Type;

	public int MaxNum;
	public int CurrentNum;

	public int MaxDurability;
	public int CurrentDurability;

	public Buff buff;
}
