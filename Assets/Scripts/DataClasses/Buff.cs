using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Buff {

	[SerializeField]
	private string _id;
	public int Str;
	public int Dex;
	public int Con;
	public int Int;
	public float Dmg;
	public float Acc;
	public float Hp;
	public float Mana;
	public float ManaRegen;
	public float HpRegen;
	public float Duration;

	public string Id {
		get {
			return _id;
		}
	}

	public Buff (string Id) {
		_id = Id;
		Str = 0;
		Dex = 0;
		Con = 0;
		Int = 0;
		Dmg = 0f;
		Acc = 0f;
		Hp = 0f;
		Mana = 0f;
		ManaRegen = 0f;
		HpRegen = 0f;
		Duration = 0f;
	}
	
	public Buff Copy () {
		Buff newBuff = new Buff(_id);

		newBuff.Str = Str;
		newBuff.Dex = Dex;
		newBuff.Con = Con;
		newBuff.Int = Int;
		newBuff.Dmg = Dmg;
		newBuff.Acc = Acc;
		newBuff.Hp = Hp;
		newBuff.Mana = Mana;
		newBuff.ManaRegen = ManaRegen;
		newBuff.HpRegen = HpRegen;
		newBuff.Duration = Duration;

		return newBuff;
	}

	public static Buff operator + (Buff buff1, Buff buff2) {
		Buff newBuff = new Buff(buff1.Id);

		newBuff.Str = buff1.Str + buff2.Str;
		newBuff.Dex = buff1.Dex + buff2.Dex;
		newBuff.Con = buff1.Con + buff2.Con;
		newBuff.Int = buff1.Int + buff2.Int;
		newBuff.Dmg = buff1.Dmg + buff2.Dmg;
		newBuff.Acc = buff1.Acc + buff2.Acc;
		newBuff.Hp = buff1.Hp + buff2.Hp;
		newBuff.Mana = buff1.Mana + buff2.Mana;
		newBuff.ManaRegen = buff1.ManaRegen + buff2.ManaRegen;
		newBuff.HpRegen = buff1.HpRegen + buff2.HpRegen;

		return newBuff;
	}

}
